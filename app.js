const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes.js');

const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.uzz5mzo.mongodb.net/S36-Discussion?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on('error', () => console.log('Connection Error'));
db.on('open', () => console.log('Connected to MongoDB!'));

//Routes
app.use('/api/tasks', taskRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));