const Task = require('../models/Task.js');

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};

module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	});

	return newTask.save().then((savedTask, error) => {
		if(error){
			return error;
		} else {
			return 'Task created successfully!';
		};
	});
};

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error;
		} else {
			result.name = newContent.name;

			return result.save().then((updatedTask, error) => {
				if(error){
					return error;
				} else {
					return updatedTask;
				};
			});
		};
	});
};

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error){
			return error;
		} else {
			return deletedTask;
		};
	});
};

module.exports.findTask = (taskId) => {
	return Task.findById(taskId).then((task, error) => {
		if(error){
			return error;
		} else {
			return task;
		};
	});
};